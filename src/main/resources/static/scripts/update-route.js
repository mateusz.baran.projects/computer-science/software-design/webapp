function addEventListener() {
    var button = document.getElementById("auto-calculate-button");

    button.addEventListener("click", function () {
        var input = document.getElementById("pointsForRouteInput");
        var height = document.getElementById("route-height");
        var length = document.getElementById("route-length");

        var h = parseFloat(height.innerHTML);
        var l = parseFloat(length.innerHTML);

        input.value = parseInt(Math.round(Math.abs(h) / 100) + Math.round(l));
    })
}


window.addEventListener("load", addEventListener);
