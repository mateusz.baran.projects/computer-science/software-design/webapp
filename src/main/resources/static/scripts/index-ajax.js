function ajaxFunction(action) {
    var idText = $('#index-info-id').text();
    var id = parseInt(idText);
    if (isNaN(id)) id = 0;

    $.ajax({
        type: 'POST',
        url: 'show-ajax',
        data: {id: id, action: action},
        success: function (response) {
            $('#index-info-id').html(response[0]);
            $('#index-info-title').html(response[1]);

            var container = $('#index-info-container');
            container.html(response[2]);
            container.addClass(response[3]);
        },
        error: function () {
            console.log("ajax error");
        }
    });
}

ajaxFunction("current");

$("#index-button-previous").click(function () {
    ajaxFunction("previous");
});

$("#index-button-next").click(function () {
    ajaxFunction("next");
});
