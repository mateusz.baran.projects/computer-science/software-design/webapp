package com.egory.eGory.repository;

import com.egory.eGory.entity.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface PointDAO extends JpaRepository<Point, Integer> {

    @Query(value = "SELECT * FROM Punkty p JOIN PunktyTrasy pt ON p.id=pt.idP JOIN PunktyTrasy pt2 ON pt.idT=pt2.idT JOIN TrasyZWydaniem tw ON pt.idT=tw.idT WHERE tw.idW=:releaseID AND pt2.idP=:pointID AND pt2.isFirst=true AND pt.isFirst=false", nativeQuery = true)
    public ArrayList<Point> findNeighbourPointsByReleaseAndPoint(int releaseID, int pointID);

    public Point findPointById(int id);
}
