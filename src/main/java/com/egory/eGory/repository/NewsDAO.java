package com.egory.eGory.repository;

import com.egory.eGory.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsDAO extends JpaRepository<News, Integer> {
}
