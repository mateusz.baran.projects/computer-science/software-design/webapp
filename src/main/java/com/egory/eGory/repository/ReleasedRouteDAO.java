package com.egory.eGory.repository;

import com.egory.eGory.entity.ReleasedRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ReleasedRouteDAO extends JpaRepository<ReleasedRoute, Integer> {
    @Query(value = "SELECT COUNT(*) FROM TrasyZWydaniem tw JOIN Wydania w ON tw.IdW = w.id WHERE tw.idT=:routeID AND w.data_wydania < CURRENT_DATE", nativeQuery = true)
    public int findCountReleasedRouteByRoute(int routeID);

    @Query(value = "SELECT COUNT(*) FROM TrasyZWydaniem tw WHERE tw.idT=:routeID AND tw.idW=:releaseID", nativeQuery = true)
    public int findCountReleasedRouteByRouteAndRelease(int routeID, int releaseID);
}
