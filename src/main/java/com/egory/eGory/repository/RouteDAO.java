package com.egory.eGory.repository;

import com.egory.eGory.entity.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.ArrayList;

public interface RouteDAO extends JpaRepository<Route, Integer> {

    @Query(value = "SELECT * FROM Trasy t JOIN TrasyZWydaniem tw ON t.id=tw.idT JOIN PunktyTrasy pt ON t.id=pt.idT JOIN PunktyTrasy pt2 ON t.id=pt2.idT WHERE tw.idW=:releaseID AND pt.idP=:pointFirstID AND pt.isFirst=true AND pt2.idP=:pointSecondID AND pt2.isFirst=false", nativeQuery = true)
    public ArrayList<Route> findRoutesByReleaseAndPoints(int releaseID, int pointFirstID, int pointSecondID);

    @Query(value = "SELECT * FROM Trasy t JOIN TrasyZWydaniem tw ON t.id=tw.idT JOIN PunktyTrasy pt ON t.id=pt.idT JOIN PunktyTrasy pt2 ON t.id=pt2.idT WHERE pt.idP=:pointFirstID AND pt.isFirst=true AND pt2.idP=:pointSecondID AND pt2.isFirst=false ORDER BY tw.id DESC", nativeQuery = true)
    public ArrayList<Route> findRoutesByPoints(int pointFirstID, int pointSecondID);

    public Route findRouteById(int id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Trasy SET szlak=:nameRoute, punkty_za_trase=:pointsForRoute, dlugosc_trasy=:routeLength, roznica_wysokosci=:heightDifference WHERE id=:idRoute", nativeQuery = true)
    public void updateRoute(int idRoute, String nameRoute, int pointsForRoute, double routeLength, int heightDifference);
}
