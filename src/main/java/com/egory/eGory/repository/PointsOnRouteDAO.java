package com.egory.eGory.repository;

import com.egory.eGory.entity.PointsOnRoute;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointsOnRouteDAO extends JpaRepository<PointsOnRoute, Integer> {
}
