package com.egory.eGory.repository;

import com.egory.eGory.entity.Release;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface ReleaseDAO extends JpaRepository<Release, Integer> {

    @Query(value = "SELECT * FROM Wydania w WHERE w.data_wydania > CURRENT_DATE ORDER BY w.id DESC", nativeQuery = true)
    public ArrayList<Release> findOpenReleases();
}
