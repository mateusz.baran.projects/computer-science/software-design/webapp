package com.egory.eGory.controller;

public class Config {
    static final int RELEASE = 2;
    static final int MAX_ROUTES_IN_TRIP = 10;
    static final String ACTIVE_PAGE = "activePage";
}
