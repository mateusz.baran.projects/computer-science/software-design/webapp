package com.egory.eGory.controller;

import com.egory.eGory.entity.News;
import com.egory.eGory.service.ServiceInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;


@Controller
public class IndexController {
    @Autowired
    ServiceInstance serviceInstance;

    @RequestMapping(value = "/")
    public String findRoute(Model model) {
        model.addAttribute("activePage", "index");

        return "index";
    }

    @RequestMapping(value = "/show-ajax", method = RequestMethod.POST)
    public @ResponseBody ArrayList<String> showAjax(@RequestParam("id") int id,
                                                    @RequestParam("action") String action) {

        ArrayList<String> list = new ArrayList<>();
        ArrayList<News> news = serviceInstance.findAllNews();

        int index;
        if ("next".equals(action)) index = id + 1;
        else if ("previous".equals(action)) index = id - 1;
        else index = id;

        if (index < 0) index = 0;
        else if (index >= news.size()) index = news.size() - 1;

        list.add(String.format("%d", index));
        list.add(news.get(index).getTitle());
        list.add(news.get(index).getContent());
        list.add(news.get(index).getStyleClass());

        return list;
    }
}
