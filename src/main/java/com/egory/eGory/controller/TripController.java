package com.egory.eGory.controller;

import com.egory.eGory.entity.Point;
import com.egory.eGory.entity.Route;
import com.egory.eGory.service.ServiceInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;

@Controller
public class TripController {
    @Autowired
    ServiceInstance serviceInstance;

    @RequestMapping(value = "/find-trip", method = RequestMethod.GET)
    public String findTrip(Model model) {
        ArrayList<Point> points = serviceInstance.findAllPoints();
        model.addAttribute("points", points);

        model.addAttribute(Config.ACTIVE_PAGE, "find-trip");
        return "find-trip";
    }

    @RequestMapping(value = "/show-trips", method = RequestMethod.POST)
    public String showTrips(Model model,
                            @RequestParam int pointFirst,
                            @RequestParam int pointSecond) {

        if (0 != pointFirst && 0 != pointSecond && pointFirst != pointSecond) {
            ArrayList<ArrayList<Point>> listOfTripPoints = getListOfTripPoints(Config.RELEASE, pointFirst, pointSecond, Config.MAX_ROUTES_IN_TRIP);
            ArrayList<ArrayList<Route>> listOfTripRoutes = getListOfTripRoutes(Config.RELEASE, listOfTripPoints);

            model.addAttribute("tripDetails", getTripDetails(listOfTripRoutes, listOfTripPoints));
        } else {
            model.addAttribute(Statement.NAME, new Statement("Niepoprawne dane!", Statement.WARNING));
            return findTrip(model);
        }

        model.addAttribute(Config.ACTIVE_PAGE, "find-trip");
        return "show-trips";
    }


    private ArrayList<ArrayList<Point>> getListOfTripPoints(int releaseID, int targetPointID, ArrayList<Point> tripPoints, int nbRoutes) {
        ArrayList<ArrayList<Point>> listOfTripPoints = new ArrayList<>();
        ArrayList<Point> neighbourPoints = serviceInstance.findNeighbourPointsByReleaseAndPoint(releaseID, tripPoints.get(tripPoints.size() - 1).getId());

        for (Point p : neighbourPoints) {
            if (!tripPoints.contains(p)) {
                ArrayList<Point> tripPointsCopy = new ArrayList<>(tripPoints);
                tripPointsCopy.add(p);

                if (p.getId() == targetPointID) {
                    listOfTripPoints.add(tripPointsCopy);
                } else if (nbRoutes > 1) {
                    listOfTripPoints.addAll(getListOfTripPoints(releaseID, targetPointID, tripPointsCopy, nbRoutes - 1));
                }
            }
        }

        return listOfTripPoints;
    }

    /**
     * @param releaseID release id
     * @param startPointID start point id
     * @param endPointID end point id
     * @param nbRoutes number of routes per trip
     * @return list of points belongs to trip
     */
    public ArrayList<ArrayList<Point>> getListOfTripPoints(int releaseID, int startPointID, int endPointID, int nbRoutes) {
        Point currentPoint = serviceInstance.findPointById(startPointID);

        ArrayList<Point> tripPoints = new ArrayList<>();
        tripPoints.add(currentPoint);

        return getListOfTripPoints(releaseID, endPointID, tripPoints, nbRoutes);
    }

    private ArrayList<ArrayList<Route>> getListOfTripRoutes(int releaseID, ArrayList<ArrayList<Point>> getListOfTripPoints) {
        ArrayList<ArrayList<Route>> listOfTripRoutes = new ArrayList<>();

        for (ArrayList<Point> tripPoints : getListOfTripPoints) {
            ArrayList<Route> tripRoutes = new ArrayList<>();

            for (int i = 0; i < tripPoints.size() - 1; i++) {
                ArrayList<Route> releasedRoutes = serviceInstance.findRoutesByReleaseAndPoints(releaseID, tripPoints.get(i).getId(), tripPoints.get(i + 1).getId());
                tripRoutes.add(releasedRoutes.get(releasedRoutes.size() - 1));
            }

            listOfTripRoutes.add(tripRoutes);
        }

        return listOfTripRoutes;
    }

    private ArrayList<ArrayList<ArrayList<HashMap<String, String>>>> getTripDetails(ArrayList<ArrayList<Route>> listOfTripRoutes, ArrayList<ArrayList<Point>> listOfTripPoints) {
        ArrayList<ArrayList<ArrayList<HashMap<String, String>>>> listOfTripInfo = new ArrayList<>();

        for (int i = 0; i < listOfTripPoints.size(); i++) {
            ArrayList<ArrayList<HashMap<String, String>>> list = new ArrayList<>();
            ArrayList<HashMap<String, String>> info = new ArrayList<>();
            ArrayList<HashMap<String, String>> details = new ArrayList<>();

            HashMap<String, String> tripInfo = new HashMap<>();
            ArrayList<Point> tripPoints = listOfTripPoints.get(i);
            ArrayList<Route> tripRoutes = listOfTripRoutes.get(i);

            double length = 0.;
            int height = 0;
            int pointsForRoute = 0;

            for (Route route : tripRoutes) {
                length += route.getRouteLength();
                height += Math.abs(route.getHeightDifference());
                pointsForRoute += route.getPointsForRoute();
            }

            tripInfo.put("tripLength", String.format("%.3f", length));
            tripInfo.put("tripHeight", String.format("%d", height));
            tripInfo.put("tripPointsForRoute", String.format("%d", pointsForRoute));

            info.add(tripInfo);


            for (int j = 0; j < tripPoints.size(); j++) {
                HashMap<String, String> tripDetails = new HashMap<>();

                Point point = tripPoints.get(j);

                tripDetails.put("pointName", point.getName());
                tripDetails.put("pointHeight", String.format("%d", point.getHeight()));

                if (j == tripRoutes.size()) {
                    tripDetails.put("isLastPoint", "true");
                } else {
                    Route route = tripRoutes.get(j);

                    tripDetails.put("isLastPoint", "false");
                    tripDetails.put("routeLength", String.format("%.3f", route.getRouteLength()));
                    tripDetails.put("routeHeight", String.format("%d", route.getHeightDifference()));
                    tripDetails.put("routePointsForRoute", String.format("%d", route.getPointsForRoute()));
                    tripDetails.put("routeName", route.getName());
                }

                details.add(tripDetails);
            }

            list.add(info);
            list.add(details);
            listOfTripInfo.add(list);
        }

        return listOfTripInfo;
    }

}
