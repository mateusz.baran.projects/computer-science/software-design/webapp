package com.egory.eGory.controller;

import com.egory.eGory.entity.*;
import com.egory.eGory.service.ServiceInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class RouteController {
    @Autowired
    ServiceInstance serviceInstance;

    @RequestMapping(value = "/find-route", method = RequestMethod.GET)
    public String findRoute(Model model) {
        ArrayList<Point> points = serviceInstance.findAllPoints();
        model.addAttribute("points", points);

        model.addAttribute(Config.ACTIVE_PAGE, "find-route");
        return "find-route";
    }

    @RequestMapping(value = "/validate-route", method = RequestMethod.POST)
    public String validateRoute(Model model,
                                @RequestParam int pointFirst,
                                @RequestParam int pointSecond) {

        if (0 == pointFirst || 0 == pointSecond || pointFirst == pointSecond) {
            model.addAttribute(Statement.NAME, new Statement("Niepoprawne dane!", Statement.WARNING));
            return findRoute(model);
        }

        Point first = serviceInstance.findPointById(pointFirst);
        Point second = serviceInstance.findPointById(pointSecond);

        ArrayList<Release> releases = serviceInstance.findOpenReleases();

        if (releases.size() == 0) {
            model.addAttribute(Statement.NAME, new Statement("Wszystkie dostętpne wersje zostały wydane!", Statement.WARNING));
            return findRoute(model);
        }

        Release release = releases.get(0);
        model.addAttribute("release", release);

        model.addAttribute("pointFirst", first);
        model.addAttribute("pointSecond", second);

        ArrayList<Route> routes = serviceInstance.findRoutesByPoints(pointFirst, pointSecond);
        if (routes.size() > 0) {
            Route route = routes.get(0);
            int isReleased = serviceInstance.findCountReleasedRouteByRoute(route.getId());
            if (isReleased > 0) route = new Route(route);

            model.addAttribute("route", route);
            model.addAttribute("action", "Aktualizuj trase");
            model.addAttribute("routePrevID", routes.get(0).getId());
        } else {
            Route route = new Route(Point.lengthBetweenPoints(first, second), Point.heightDifferenceBetweenPoints(first, second));

            model.addAttribute("route", route);
            model.addAttribute("action", "Dodaj trase");
            model.addAttribute("routePrevID", 0);
        }

        model.addAttribute("routeNames", Route.ROUTE_NAMES);
        model.addAttribute(Config.ACTIVE_PAGE, "find-route");
        return "update-route";
    }

    @RequestMapping(value = "/add-route-to-db", method = RequestMethod.POST)
    public String addRouteToDB(Model model,
                               @Valid @ModelAttribute Route route,
                               BindingResult errors,
                               @RequestParam int pointFirstID,
                               @RequestParam int pointSecondID,
                               @RequestParam int releaseID,
                               @RequestParam int routePrevID) {
        if (errors.hasErrors()) {
            Point first = serviceInstance.findPointById(pointFirstID);
            Point second = serviceInstance.findPointById(pointSecondID);
            ArrayList<Release> releases = serviceInstance.findOpenReleases();

            model.addAttribute("release", releases.get(0));
            model.addAttribute("pointFirst", first);
            model.addAttribute("pointSecond", second);

            if (routePrevID == 0) model.addAttribute("action", "Dodaj trase");
            else model.addAttribute("action", "Aktualizuj trase");

            model.addAttribute("routeNames", Route.ROUTE_NAMES);
            return "update-route";
        }

        if (route.getId() != 0) {
            serviceInstance.updateRoute(route.getId(), route.getName(), route.getPointsForRoute(), route.getRouteLength(), route.getHeightDifference());
            model.addAttribute(Statement.NAME, new Statement("Zmiany zostały zaktualizowane.", Statement.CONFIRMATION));
        } else {
            Route routePrev = serviceInstance.findRouteById(routePrevID);

            if (routePrev != null && route.compare(routePrev)) {
                route = routePrev;
            } else {
                serviceInstance.saveRoute(route);

                PointsOnRoute pointFirst = new PointsOnRoute(pointFirstID, route.getId(), true);
                PointsOnRoute pointSecond = new PointsOnRoute(pointSecondID, route.getId(), false);

                serviceInstance.savePointOnRoute(pointFirst);
                serviceInstance.savePointOnRoute(pointSecond);
            }

            int isReleased = serviceInstance.findCountReleasedRouteByRouteAndRelease(route.getId(), releaseID);
            if (isReleased == 0) {
                model.addAttribute(Statement.NAME, new Statement("Zmiany zostaną wprowadzone w nowym wydaniu.", Statement.CONFIRMATION));
                ReleasedRoute releasedRoute = new ReleasedRoute(releaseID, route.getId());
                serviceInstance.saveReleasedRoute(releasedRoute);
            } else {
                model.addAttribute(Statement.NAME, new Statement("Zmiany nie zostały wprowadzone!", Statement.WARNING));
            }
        }

        return findRoute(model);
    }
}
