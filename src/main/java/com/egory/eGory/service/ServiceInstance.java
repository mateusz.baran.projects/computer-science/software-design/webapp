package com.egory.eGory.service;

import com.egory.eGory.entity.*;
import com.egory.eGory.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ServiceInstance implements ServiceInstanceInterface {
    @Autowired
    RouteDAO routeDAO;

    @Autowired
    PointDAO pointDAO;

    @Autowired
    ReleasedRouteDAO releasedRouteDAO;

    @Autowired
    ReleaseDAO releaseDAO;

    @Autowired
    PointsOnRouteDAO pointsOnRouteDAO;

    @Autowired
    NewsDAO newsDAO;


    @Override
    public ArrayList<Route> findRoutesByReleaseAndPoints(int releaseID, int pointFirstID, int pointSecondID) {
        return routeDAO.findRoutesByReleaseAndPoints(releaseID, pointFirstID, pointSecondID);
    }

    @Override
    public ArrayList<Route> findRoutesByPoints(int pointFirstID, int pointSecondID) {
        return routeDAO.findRoutesByPoints(pointFirstID, pointSecondID);
    }

    @Override
    public Route findRouteById(int id) {
        return routeDAO.findRouteById(id);
    }

    @Override
    public void updateRoute(int idRoute, String nameRoute, int pointsForRoute, double routeLength, int heightDifference) {
        routeDAO.updateRoute(idRoute, nameRoute, pointsForRoute, routeLength, heightDifference);
    }

    @Override
    public void saveRoute(Route route) {
        routeDAO.save(route);
    }

    @Override
    public void savePointOnRoute(PointsOnRoute pointsOnRoute) {
        pointsOnRouteDAO.save(pointsOnRoute);
    }

    @Override
    public void saveReleasedRoute(ReleasedRoute releasedRoute) {
        releasedRouteDAO.save(releasedRoute);
    }

    @Override
    public ArrayList<Point> findNeighbourPointsByReleaseAndPoint(int releaseID, int pointID) {
        return pointDAO.findNeighbourPointsByReleaseAndPoint(releaseID, pointID);
    }

    @Override
    public Point findPointById(int id) {
        return pointDAO.findPointById(id);
    }

    @Override
    public ArrayList<Point> findAllPoints() {
        return (ArrayList<Point>) pointDAO.findAll();
    }

    @Override
    public ArrayList<Release> findOpenReleases() {
        return releaseDAO.findOpenReleases();
    }

    @Override
    public int findCountReleasedRouteByRoute(int routeID) {
        return releasedRouteDAO.findCountReleasedRouteByRoute(routeID);
    }

    @Override
    public int findCountReleasedRouteByRouteAndRelease(int routeID, int releaseID) {
        return releasedRouteDAO.findCountReleasedRouteByRouteAndRelease(routeID, releaseID);
    }

    @Override
    public ArrayList<News> findAllNews() {
        return (ArrayList<News>) newsDAO.findAll();
    }
}
