package com.egory.eGory.service;

import com.egory.eGory.entity.*;

import java.util.ArrayList;

public interface ServiceInstanceInterface {
    ArrayList<Route> findRoutesByReleaseAndPoints(int releaseID, int pointFirstID, int pointSecondID);

    ArrayList<Route> findRoutesByPoints(int pointFirstID, int pointSecondID);

    Route findRouteById(int id);

    void updateRoute(int idRoute, String nameRoute, int pointsForRoute, double routeLength, int heightDifference);

    void saveRoute(Route route);

    ArrayList<Point> findNeighbourPointsByReleaseAndPoint(int releaseID, int pointID);

    Point findPointById(int id);

    ArrayList<Release> findOpenReleases();

    int findCountReleasedRouteByRoute(int routeID);

    int findCountReleasedRouteByRouteAndRelease(int routeID, int releaseID);

    ArrayList<Point> findAllPoints();

    void savePointOnRoute(PointsOnRoute pointsOnRoute);

    void saveReleasedRoute(ReleasedRoute releasedRoute);

    ArrayList<News> findAllNews();
}
