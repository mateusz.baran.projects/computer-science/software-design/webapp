package com.egory.eGory.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "PunktyTrasy")
public class PointsOnRoute implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "isFirst")
    boolean isFirst;

    @Column(name = "idP")
    int pointId;

    @Column(name = "idT")
    int routeId;

    public PointsOnRoute() {
    }

    public PointsOnRoute(int pointId, int routeId, boolean isFirst) {
        this.pointId = pointId;
        this.routeId = routeId;
        this.isFirst = isFirst;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public int getPointId() {
        return pointId;
    }

    public void setPointId(int pointId) {
        this.pointId = pointId;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }
}
