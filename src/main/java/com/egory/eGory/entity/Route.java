package com.egory.eGory.entity;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Trasy")
public class Route {

    public static final ArrayList<String> ROUTE_NAMES = new ArrayList<String>() {{
        add("czerwony");
        add("niebieski");
        add("żółty");
        add("zielony");
        add("czarny");
    }};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @NotNull
    @Column(name = "szlak")
    String name;

    @Min(value = 0, message = "liczba punktów nie może być poniżej 0")
    @Max(value = 100, message = "liczba punktów nie może być powyżej 100")
    @NotNull
    @Column(name = "punkty_za_trase")
    int pointsForRoute;

    @Column(name = "dlugosc_trasy")
    double routeLength;

    @Column(name = "roznica_wysokosci")
    int heightDifference;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "PunktyTrasy",
            joinColumns = @JoinColumn(name = "idT"),
            inverseJoinColumns = @JoinColumn(name = "idP"))
    private List<Point> points = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "TrasyZWydaniem",
            joinColumns = @JoinColumn(name = "idT"),
            inverseJoinColumns = @JoinColumn(name = "idW"))
    private List<Release> releases = new ArrayList<>();

    public Route() {
    }

    public Route(String name, int pointsForRoute, double routeLength, int heightDifference) {
        this.name = name;
        this.pointsForRoute = pointsForRoute;
        this.routeLength = routeLength;
        this.heightDifference = heightDifference;
    }

    public Route(Route route) {
        this.name = route.name;
        this.pointsForRoute = route.pointsForRoute;
        this.routeLength = route.routeLength;
        this.heightDifference = route.heightDifference;
    }

    public Route(double routeLength, int heightDifference) {
        this.routeLength = routeLength;
        this.heightDifference = heightDifference;
    }

    /**
     * @param route route to compare
     * @return true if has same fields otherwise false
     */
    public boolean compare(Route route) {
        return this.name.equals(route.name) &&
                this.pointsForRoute == route.pointsForRoute &&
                this.routeLength == route.routeLength &&
                this.heightDifference == route.heightDifference;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPointsForRoute() {
        return pointsForRoute;
    }

    public void setPointsForRoute(int pointsForRoute) {
        this.pointsForRoute = pointsForRoute;
    }

    public double getRouteLength() {
        return this.routeLength;
    }

    public void setRouteLength(double routeLength) {
        this.routeLength = routeLength;
    }

    public int getHeightDifference() {
        return heightDifference;
    }

    public void setHeightDifference(int heightDifference) {
        this.heightDifference = heightDifference;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public List<Release> getReleases() {
        return releases;
    }

    public void setReleases(List<Release> releases) {
        this.releases = releases;
    }
}
