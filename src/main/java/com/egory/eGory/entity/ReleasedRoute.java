package com.egory.eGory.entity;

import javax.persistence.*;

@Entity(name = "TrasyZWydaniem")
public class ReleasedRoute {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "idW")
    int releaseId;

    @Column(name = "idT")
    int routeId;

    public ReleasedRoute() {
    }

    public ReleasedRoute(int releaseId, int routeId) {
        this.releaseId = releaseId;
        this.routeId = routeId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getReleaseId() {
        return releaseId;
    }

    public void setReleaseId(int releaseId) {
        this.releaseId = releaseId;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }
}
