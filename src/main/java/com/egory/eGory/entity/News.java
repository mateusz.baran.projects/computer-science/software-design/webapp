package com.egory.eGory.entity;

import javax.persistence.*;

@Entity(name = "Aktualnosci")
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "tytul")
    private String title;

    @Column(name = "zawartosc", length=65536)
    private String content;

    @Column(name = "klasa_stylu")
    private String styleClass;

    public News() {
    }

    public News(String title, String content, String styleClass) {
        this.title = title;
        this.content = content;
        this.styleClass = styleClass;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }
}
