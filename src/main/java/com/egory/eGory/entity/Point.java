package com.egory.eGory.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Punkty")
public class Point {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nazwa")
    private String name;

    @Column(name = "wysokosc")
    private int height;

    @Column(name = "dl_geo")
    private double longitude;

    @Column(name = "szer_geo")
    private double latitude;

    @Column(name = "opis")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "PunktyTrasy",
            joinColumns = @JoinColumn(name="idP"),
            inverseJoinColumns = @JoinColumn(name = "idT"))
    private List<Route> routes = new ArrayList<>();

    public Point() {
    }

    public Point(String name, int height, double longitude, double latitude, String description) {
        this.name = name;
        this.height = height;
        this.longitude = longitude;
        this.latitude = latitude;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    /**
     * @param first first point
     * @param second second point
     * @return distance between points
     */
    public static double lengthBetweenPoints(Point first, Point second) {
        return Math.sqrt(Math.pow(first.getLatitude() - second.getLatitude(), 2) + Math.pow(Math.cos(second.getLatitude() * Math.PI / 180) * (first.getLongitude() - second.getLongitude()), 2)) * 40075.704 / 360.0;
    }

    /**
     * @param first first point
     * @param second second point
     * @return height difference between points
     */
    public static int heightDifferenceBetweenPoints(Point first, Point second) {
        return second.getHeight() - first.getHeight();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Point{" +
                ", name='" + name + '\'' +
                ", height=" + height +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", description='" + description + '\'' +
                '}';
    }
}