package com.egory.eGory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EGoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(EGoryApplication.class, args);
	}

}

