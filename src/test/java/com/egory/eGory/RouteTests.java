package com.egory.eGory;

import com.egory.eGory.entity.Route;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RouteTests {
    Route routeFirst;
    Route routeSecond;
    Route routeThird;

    @Before
    public void setUp() {
        routeFirst = new Route("czerwony", 7, 5.323, 212);
        routeSecond = new Route("czerwony", 7, 5.323, 212);
        routeThird = new Route("czerwony", 7, 5.32, 212);
    }

    @Test
    public void lengthBetweenPointsTest() {
        Assert.assertTrue(routeFirst.compare(routeSecond));
        Assert.assertFalse(routeFirst.compare(routeThird));
    }
}
