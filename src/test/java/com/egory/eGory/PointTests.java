package com.egory.eGory;

import com.egory.eGory.entity.Point;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PointTests {
    Point pointFirst;
    Point pointSecond;

    @Before
    public void setUp() {
        pointFirst = new Point("Rysy", 2503, 20.088333, 49.179444, "Szczyt górski");
        pointSecond = new Point("Śnieżka", 1603, 15.739167, 50.735833, "Szczyt górski");
    }

    @Test
    public void lengthBetweenPointsTest() {
        Assert.assertEquals(Point.lengthBetweenPoints(pointFirst, pointSecond), 352.011, 1e-3);
    }

    @Test
    public void heightDifferenceBetweenPointsTest() {
        Assert.assertEquals(Point.heightDifferenceBetweenPoints(pointFirst, pointSecond), -900);
    }
}
